import numpy as np

# Neural network using only matrices from numpy
#three layers- layer 1:input , layer 2: hidden , layer 3: output


def sigfun(qq): #sigmoid function
    return 1/(1+np.exp(-qq));
def dsigfun(qq): #derivative of sigmoid function
    return sigfun(qq) * (1 - sigfun(qq))

x = np.array([[0,0,1],
              [0,1,1],
              [1,0,1],
              [0,1,0],
              [1,1,1],
              [0,0,0]
                      ])
#x input 

y = np.array([[0,1,1,1,0,0]]).T
#y expected output

hunits = 5; # number of neurons or units in the hidden layer
NofInputs = int(x.shape[0]); #number of inputs or outputs used for training

w1 = np.random.random((3,hunits));
#w1 randomized weights : used to convert input to hidden
#print(w1)
b1 = np.zeros((NofInputs,hunits));
#b1 bias unit, same size as input layer, same "b" for all neurons in this layer. 
#Another method is to add 1 as extra feature in input, [1,x1,x2,x3] and "b" in the weights of the next layer [b,w1,w2,w3].T
#for each neuron in the next layer [1,x1,x2,x3] * [b,w1,w2,w3].T = [b + (Sum(Wx)) ]

w2 = np.random.random((hunits,1));
#w2 randomized weights : used to convert hidden to output
#print(w2)
b2 = np.zeros((NofInputs,1));
#b2 bias unit


for i in range(600000):
    
    #forward propagation
    
    l2 = np.dot(x,w1) + b1; #preprocessing layer 2
 
    hid = sigfun(l2); #hidden layer or layer 2
    
    l3 = np.dot(hid,w2) + b2; #preprocessing layer 3
    
    out = sigfun(l3); #output layer or layer 3
    
    #backpropagation
    
    dout = dsigfun(l3); #derivative of sigmoid function for output layer
     
    error2 = (y - out); #error : goal for y == out so error is 0
       
    delta2 = np.multiply(error2,dout); 
    db2 = delta2; #delta to shift bias unit
    dw2 = np.dot(hid.T,delta2); #delta to shift weights
    
    dhid =  dsigfun(l2); #derivative of sigmoid function for hidden layer

    error1 = np.dot(delta2,w2.T) #propagated error from output error
    
    delta1 = np.multiply(error1,dhid); 
    db1 = delta1; #delta to shift bias unit
    dw1 = np.dot(x.T,delta1); #delta to shift weights

    step = 0.5
    
    w1 += step * dw1; #shifting weights and bias units
    w2 += step * dw2; 
    b1 += step * b1;
    b2 += step * b2;
    
b1 = b1.mean(0); #average bias 
b2 = b2.mean(0); 

#using new weights predict input [1, 1, 0] or [1, 0, 0] which are not included in training
test = np.array([1, 1, 0]);
feedfwd = sigfun(np.dot(sigfun(np.dot(test,w1) + b1),w2)+b2); #feedforward test input
print(feedfwd) #if properly trained, output of [1, 1, 0] should be 0



